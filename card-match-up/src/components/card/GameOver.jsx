import React from 'react';

const GameOver = ({ restartGame }) => (
  <div className="justify-center">
    <h1>YOU WIN!!</h1>
    <h3>For more games www.clickjogos.com</h3>
    <button className="restart-button" onClick={restartGame}>Restart Game</button>
  </div>
);

export default GameOver;